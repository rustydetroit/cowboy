export class MainController {
  constructor ($http,$log) {
    'ngInject';

    this.$http  = $http;
    this.$log   = $log;
    this.last   = null;
    this.id     = null;
    this.found  = null;
    this.searching  = false;

    this.mock   = [
      {
        first: 'mary',
        last: 'sure',
        code: '0921384'
      },
      {
        first: 'mary',
        last: 'sure',
        code: '0921384'
      },
      {
        first: 'mary',
        last: 'sure',
        code: '0921384'
      },
      {
        first: 'mary',
        last: 'sure',
        code: '0921384'
      },
      {
        first: 'mary',
        last: 'sure',
        code: '0921384'
      }
    ];

    this.results = [];

    //this.found = true; this.results = this.mock;
  }

  getData()
  {
    this.searching = true;
    let id    = this.id;
    let last  = this.last;
    let url   = `data.php?id=${id}&last=${last}`;

    // Mocking
    //this.found = false; return false;
    //this.results = this.mock; this.searching = false; this.found = true; return true;

    this.$http.get(url)
      .then((response) => {
        if(response.data.length > 0){
          this.found    = true;
          this.searching = false;
          this.results  = response.data;
        } else {
          this.searching = false;
          this.found    = false;
          this.results  = [];
        }
      })
      .catch((error) => {
        this.found    = false;
        this.$log.error('XHR Failed for ' + url + ' ' + angular.toJson(error.data, true));
      });
  }
}
