<?php

    $servername = "us-prod-mysql-adhoc.atl01.stelladotops.com";
    $username = "stella_reader";
    $password = "exo9thaC";
    $dbname = "productiondotdollars";

    $stylistId  = (int) !(empty($_GET['id'])) ? htmlentities(trim($_GET['id'])) : 007;
    $stylistLast = !(empty($_GET['last'])) ? strtolower(htmlentities(trim($_GET['last']))) : 'bar';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    

    $sql = "SELECT * FROM dotdollars_codes WHERE `customer_id` = $stylistId AND lower(stylist_last) = lower(\"$stylistLast\") ORDER BY `customer_last` ASC";
    //die($sql);
    $result = $conn->query($sql);
    $out    = array();

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $out[]  = array(
                'first' => $row['customer_first'],
                'last'  => $row['customer_last'],
                'code'  => $row['coupon_code']
            );
        }
    }

    $now = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']); 
    $success = (count($out) > 0) ? 'Success' : 'Failure';
    $file = 'cowboy.log';
    // Open the file to get existing content 
    $current = file_get_contents($file);
    // Append a new person to the file
    $current .= "$now : $success : $stylistId : $stylistLast \n";
    // Write the contents back to the file
    file_put_contents($file, $current);

    echo json_encode($out);

    $conn->close();
?>
